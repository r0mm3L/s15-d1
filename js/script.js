

let favFood = 'siomai';

console.log(favFood);

let sumAdd = 150+9;

console.log(sumAdd);

let product = 100 * 90;

console.log(product);

let userActive = true;

console.log(userActive);

let restaurants = ["dimsumbreak", "chixboy", "heirloom", "Charlies cup", "ramen house"];

console.log(restaurants);

let favSinger = {
	firstName: "Chris",
	lastName: "Tomlin",
	stageName: "Chris",
	birthDay: "May 4, 1972",
	age: "49",
	bestAlbum: "hello love",
	bestSong: "How great is our God",
	isActive: true

}

console.log(favSinger);


function quo(num1, num2) {
	return(num1 / num2);

}

let quotient = quo(10, 2);

console.log("The result of the division is:" + " " + quotient);


// mathematical operators

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

num1 += num4;

console.log(num1);


num2 += num4;
console.log(num2);

// num1 = num1 * 2;

num1 *= 2;

console.log(num1);

let string1 = "Boston ";
let string2 = "Celtics";


string1 += string2;

console.log(string1);

num1 -= string1;

console.log(num1);


let string3 = "hello everyone";
let myArray = string3.split("", 3);
console.log(myArray);

// math operations - follows mdas

let mdasResult = 1 + 2 - 3 * 4 / 5;

/*

3*4 = 12
12/5=2.4
1+2=3
3-2.4=0.6


*/

console.log(mdasResult);


// pemdas - parenthesis, exponents, multiplication, division, addition and subtraction

let pemdasResult = 1 + (2-3) * (4/5);

/*

4/5 = 0.8
2-3= -1
-1*0.8 = -0.8
1+ -0.8 = .0


*/


console.log(pemdasResult);

// increment and decrement
// two types increment: pre-fix and post-fix


let z = 1;


//pre-fix incrementation

++z;

console.log(z);

//post-fix incrementation

z++;

console.log(z); 

// previous value  is returned (2)  before incremented.

console.log(z++); // this is sitll 3. because the incremeneted is not returned yet
console.log(z); // this is already 4  because the new value of is already returned


let n = 1 
console.log(++n);
console.log(n);


console.log(n++);
console.log(n);



console.log(z);
console.log(z--);
console.log(z);


// comparison Operators  - used to compare values
// equality or loose equality operator (==)


console.log(1 == 1); // true
console.log(`1` == 1); // tru -  only checking the value


// strict equality


console.log(1 === 1);
console.log(`1` ===1);  // false - checking the data type not just the value


console.log('apple' == 'Apple') // false - value not equal. case sensitive
console.log('apple' === 'Apple') // still false - same data type but still different value - case sensitive. 
 
let isSame = 55 == 55;

console.log(isSame);


 console.log(0 == false); // true - force coercion 

 console.log(1 == true);

 console.log(true == 'true'); // 1 + NaN


 console.log(true == '1'); // true - 'true' is forced coerced to 1.

 console.log('0' == false); // true - 'false' is forced coerced to 0


// strict equality -- checks both value and type

console.log(1 === '1'); 

console.log('Juan' === 'Juan')

console.log('Maria' === 'maria')

 console.log('0' === false); // testing - false


 // Inequality operators (!=)

 // checks whether the operands are not equal and/or have different value.


 console.log('1' != 1); // false

 // false > both are converted to numbers 
 // '1' converted to number is 1
 //1 is coverted  into numer is 1
 // 1 ==1 
 // not equal

 console.log('James' != 'John')

 console.log(1 != true); // false

 console.log(1 != "true"); //true 

 //with type conversion: true was converted to 1
 //"true" was converted to a number but results to nan
 //1 is not eual to nan
 // it is inequal.


 // strict inqeuality operator (!==) > it checks the whether the two operand have different and will check if they have different types

 console.log('5' !== 5); //true
 console.log(5 !== 5); // false

 let name1 = 'Juan';
 let name2 = 'Maria';
 let name3 = 'Pedro';
 let name4 = 'Perla';

 let number1 = 50;
 let number2 = 60;
 let numString1 = "50";
 let numString2 = "60";

 // console.log(numString1 == number1);



// relational comparison operators
// a comparison operator checks the relationship between 

 let x = 500;
 let y = 700;
 let w = 8000;
 let numString3 = "5500";

 // creater than (>)

 console.log( x > y); 
console.log(w > y);

// less than (<)

console.log(y < y); // false. Because 700 < 700. the right side is not exactly 700, it is actually 699. it can only be exact 700 if we compare 700 <=(less then or eual) 700.
console.log(y >= y); //false
console.log(x < 1000);
console.log(numString3 < 1000); //false
console.log(numString3 < 6000); //true
console.log(numString3 < "Juan"); // true - that is erratic - logical error
console.log(6000 < 'juan'); // false - erratic again


// logical operators

	// and operator (&&) - both operands on the left and right or all operands added must be true or otherwise it should be false. 

	// T && T = T
	// T && F = F
	// F && T = F
	// F && F = F

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1); //false

	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2); //true

	let authorization3 = isAdmin && isLegalAge;
	console.log(authorization3); //false


	let requiredLevel = 95;
	let requiredAge = 18;

	let authorization4 = isRegistered && requiredLevel === 25;
	console.log(authorization4); // false
	let authorization5 = isRegistered && isLegalAge && requiredLevel == 95;
	console.log(authorization5);

	let userName1 = 'gamer2021';
	let userName2 = 'shadowMsdyrt';
	let userAge1 = 15;
	let userAge2 = 30;

	let registration1 = userName1.length > 9 && userAge1 >= requiredAge;


	// ".length" is a property of strings which determine the number of characters in the string

	console.log(registration1) // false


	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2);

	let registration3 = userName1.length > 8 && userAge2 >= requiredAge;
	console.log(registration3);




	let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin) // false

	// not operator (!)

	// it turns a boolean into the opposite value: T = F F = T

	let guildAdmin2 = !isAdmin || userLevel2 >= requiredlevel;
	console.log(guildAdmin); //true

	let opposite1 = !isAdmin;
	let opposite2 = !isLegalAge;

	console.log(opposite1); // true - isAdmin original vallue = false
	console.log(opposite2); // false - isLegallAge original vaalue = true

	// if

	const candy = 100;
	if (candy >= 100) {
		console.log('You got a cavity!')
	}

/*

if(true) {
	block of code
};

*/

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge = 30;

if(userName3.length > 10) {
	console.log("Welcome to the Game online!")
}




// else statemeent will run if the conditoon givee is false of results to false

if(userName3 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining Noobis Guild");
} 
else
{
	console.log("You are too strong to be noob. :(");

};

// else if - else if the executes a statement, if the previous or the original condition is false or resulted to false but another specified
// condition is false or result to false but another specified condition resulted to true.

if(userName3.lenth >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the noobies guild.");
}
else if(userLevel3 > 25) {
	console.log("You too strong to be noob.");
}
else if(userAge3 < requiredAge) {
	console.log("You're too young to join the guild.");
}
else 
{
	console.log("End of the condition.");
}

// if else in function

function addNum(num1, num2) {
	if(typeof num1 == "number" && typeof num2 == "number") {
		console.log("Run only if both arguments passed are number types.");
		console.log(num1 + num2);
	} 
	else {
	console.log("One or both of the arguments are not numbers")
	}; 
};

























